First install lib with :
`$ pip install -r requirements.txt`

Set on Windows :
`$ set FLASK_APP=src/config_app.py`

Or on Ubuntu :
`$ export FLASK_APP=src/config_app.py`

Then:
`$ flask run`
If it doesn't work :
`$ python3 -m flask run`