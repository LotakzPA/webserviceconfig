from flask import Flask, request
from generate_conf import GenerateConf


app = Flask(__name__)


@app.route('/<config_file>', methods=['POST'])
def hello_world(config_file):
    ''''Entry point'''
    generated_conf = GenerateConf.generate(config_file, request.get_json())

    return generated_conf
