class GenerateConf:

    @staticmethod
    def generate(template_file_name, json_config):
        with open('../ressources/' + template_file_name, 'r') as config_template:
            spam_reader = config_template.read()
            nginx_config = spam_reader.format(**json_config['configs'])
            return nginx_config
