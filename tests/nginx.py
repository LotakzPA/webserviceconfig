import unittest
from src.generate_conf import GenerateConf
import json


class MyTestCase(unittest.TestCase):
    def test_generate_conf(self):
        with open('ressources/nginx.conf') as expectation:
            with open('ressources/nginx_config.json', 'r') as json_config:
                reality = GenerateConf.generate('nginx.conf.tmpl', json.load(json_config))
                self.assertEqual(expectation.read(), reality)


if __name__ == '__main__':
    unittest.main()
